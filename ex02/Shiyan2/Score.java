import java.util.Scanner;
class Score {
	private int pnum;
	private double[] grade = null;
	public Score(){
		
	}
	public void setdata(int pnum,double[] grade){
		this.pnum=pnum;
		this.grade=grade;
	}
	public void setpnum(int pnum){
		this.pnum=pnum;
	}
	public void setgrade(double[] grade){
		this.grade=grade;
	}
	public int getpnum(){
		return pnum;
	}
	public double[] getgrade(){
		return grade;
	}
	public void intputScore(double score,int i){
			grade[i]=score;
	}
	public double average(double score[]){
		double ave,sum=0;
		for(int i=0;i<score.length;i++){
			sum+=score[i];
		}
		ave=(sum-(getmax(getgrade()))-(getmin(getgrade())))/(double)(score.length-2);
		return ave;
	}
	public double getmax(double score[]){
		double max=score[0];
		for(int i=1;i<score.length;i++){
			if(max<score[i])
				max=score[i];
		}
		return max;
	}
	public double getmin(double score[]){
		double min=score[0];
		for(int i=1;i<score.length;i++){
			if(min>score[i])
				min=score[i];
		}
		return min;
	}

}
