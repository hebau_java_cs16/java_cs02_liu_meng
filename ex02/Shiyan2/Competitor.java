import java.util.Arrays;
class Competitor implements Comparable<Competitor>{
	private String no;
	private String name;
	private double grade;
	public Competitor(){//定义构造方法
		
	}
	public Competitor(String no,String name){//定义构造方法
		this.no=no;
		this.name=name;
	}
	public void setno(String no){
		this.no=no;
	}
	public void setname(String name) {
		this.name=name;
	}
	public void setg(double grade) {
		this.grade=grade;
	}
	public String getno(){
		return this.no;		
	}
	public String getname(){
		return this.no;		
	}
	public double getgrade(){
		return this.grade;		
	}
	public String toString(){
		return "选手姓名："+name+"\t选手编号"+no+"\t选手得分"+grade;
	}
	public int compareTo(Competitor o) {
		if(this.grade > o.grade) {
			return -1;
		}
		else if(this.grade <o.grade ) {
			return 1;
		}
		else {
			return 0;
		}
	}

}