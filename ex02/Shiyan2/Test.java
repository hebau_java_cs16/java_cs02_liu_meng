import java.util.Arrays;//出现空指针异常问题,原因是没有将对象数组实例化

import java.util.Scanner;
public class Test {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int num;
		System.out.println("请问有几位选手：");
		num=in.nextInt();
		Score Pingwei = new Score();//定义评委的基本信息即score类
		System.out.println("请问有几位评委？");
		Pingwei.setpnum(in.nextInt());
		Competitor[] people = new Competitor[num];//定义选手的对象数组
		double ave[] = new double[num];//定义每位选手的平均分即最终得分
		for(int i=0;i<num;i++){//记录每位选手的个人信息
			people[i] = new Competitor();
			System.out.println("请输选手编号：");
			people[i].setno(in.next());
			System.out.println("请输入选手姓名：");
			people[i].setname(in.next());
		}
		for(int i=0;i<num;i++) {    //得到每位选手的最终得分
			double[] grade = new double[Pingwei.getpnum()];
			Pingwei.setgrade(grade);
			System.out.println("请"+Pingwei.getpnum()+"位评委输入"+people[i].getno()+"选手的成绩：");
			for(int j=0;j<Pingwei.getpnum();j++)
			Pingwei.intputScore(in.nextDouble(), j);
			System.out.println("编号为"+people[i].getno()+"的选手的每个评委给出的评分为：");
			for(int j=0;j<Pingwei.getpnum();j++){
				System.out.println(Pingwei.getgrade()[j]);
			}
			 System.out.println("最high分："+Pingwei.getmax(Pingwei.getgrade()));
			 System.out.println("最low分："+Pingwei.getmin(Pingwei.getgrade()));
			 ave[i] = Pingwei.average(Pingwei.getgrade());//得到最终得分
			 System.out.println("平均分："+ave[i]);
		}
		
		for(int i=0;i<people.length;i++) {//录入选手得分
			people[i].setg(ave[i]);	
		}
		 Arrays.sort(people); 
		 for(int i=0;i<people.length;i++) {
			 System.out.println("第"+(i+1)+"名\t"+people[i].toString());
		 }		
	}
	
}
