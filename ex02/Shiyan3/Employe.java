
public class Employe {
	private String Eno=null;
	private String name = null;
	private String sex= null;
	private Data brithday= null;
	private String workplace = null;
	private Data worktime = null;
	public Employe() {
		
	}
	public Employe(String Eno,String name,String sex,Data brithday,String workplace,Data worktime){
		this.Eno=Eno;
		this.name=name;
		this.sex=sex;
		this.brithday=brithday;
		this.workplace=workplace;
		this.worktime=worktime;
	}
	public void setEno(String Eno) {
		this.Eno=Eno;
	}
	public String getEno() {
		return Eno;
	}
	public void setname(String name) {
		this.name=name;
	}
	public String getname() {
		return name;
	}
	public void setsex(String sex) {
		this.sex=sex;
	}
	public String getsex() {
		return sex;
	}
	public void setworkplace(String workplace) {
		this.workplace=workplace;
	}
	public String getworkplace() {
		return workplace;
	}
	public void setbrithday(Data birthday) {
		this.brithday=birthday;
	}
	public String getbrithday() {
		return brithday.toString();
	}
	public void setworktime(Data worktime) {
		this.worktime=worktime;
	}
	public String getworktime() {
		return worktime.toString();
	}
	public String toString() {
		return Eno+"\t"+name+"\t"+sex+"\t"+brithday.toString()+"\t"+workplace+"\t"+worktime.toString();
	}
}

