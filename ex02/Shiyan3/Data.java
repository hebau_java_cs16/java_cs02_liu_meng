
public class Data {
	private	int year;
	private	int month;
	private	int day;
	public Data(int year,int month,int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}
	public String toString() {
		return year+"-"+month+"-"+day;
	}
	public Data() {
		
	}
	public void setyear(int year) {
		this.year=year;
	}
	public void setmonth(int month) {
		this.month = month;
	}
	public void setday(int day) {
		this.day = day;
	}
}
