import java.util.Scanner;
public class Test1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Employe[] employee = new Employe[10];//对象数组中的每个元素初始化都需要单独将其实例化，因为每一个都是一个对象
		//单独实例化
		employee[0] = new Employe("101","李华","男",new Data(1986,10,25),"开发部",new Data(2000,8,29));
		employee[1] = new Employe("102","魏紫萱","男",new Data(1988,1,5),"市场部",new Data(2001,6,1));
		employee[2] = new Employe("103","赵欣茜","女",new Data(1997,11,2),"开发部",new Data(2019,8,7));
		employee[3] = new Employe("104","刘萌","男",new Data(1998,12,8),"开发部",new Data(2019,8,9));
		employee[4] = new Employe("105","张楠","男",new Data(1989,5,15),"市场部",new Data(2002,7,26));
		employee[5] = new Employe("106","小明","男",new Data(1986,4,28),"市场部",new Data(2000,10,23));
		employee[6] = new Employe("109","李嵘","女",new Data(1999,6,21),"市场部",new Data(2019,8,9));
		employee[7] = new Employe("100","赵欣","女",new Data(1994,7,10),"开发部",new Data(2018,7,15));
		employee[8] = new Employe("110","万茜","男",new Data(1990,3,14),"市场部",new Data(2011,9,19));
		employee[9] = new Employe("153","小红","女",new Data(1992,1,25),"开发部部",new Data(2010,8,1));
	//企图从键盘输入，也可以运行就是emmmm有点累
	/*	for(int i=0;i<employee.length;i++) {
			System.out.println("请输入这位员工的姓名(第一位和第二位为部门经理)");
			String name = in.next();
			System.out.println("请输入这位员工的职工号");
			String no = in.next();
			System.out.println("请输入这位员工的性别");
			String sex = in.next();
			System.out.println("请输入这位员工的生日");
			int year1=in.nextInt();
			int	month1=in.nextInt();
			int day1=in.nextInt();
			Data birthday = new Data(year1,month1,day1);
			birthday.setyear(year1);
			birthday.setmonth(month1);
			birthday.setday(day1);
			System.out.println("请输入这位员工的工作部门");
			String workplace = in.next();
			System.out.println("请输入这位员工的工作日期");
			int	year2=in.nextInt();
			int	month2=in.nextInt();
			int	day2=in.nextInt();
			Data worktime = new Data(year2,month2,day2);
			
			employee[i] =new Employe(no,name,sex,birthday,workplace,worktime);
		}
		*/	
		Dpartment[] dpartment = new Dpartment[2];
		dpartment[0]=new Dpartment("001",employee[0].getworkplace(),employee[0]);
		dpartment[1]=new Dpartment("002",employee[1].getworkplace(),employee[1]);
		for(int i=0;i<dpartment.length;i++) {
			System.out.println(dpartment[i].toString());
			System.out.println("员工编号\t"+"员工姓名\t"+"性别\t"+"生日\t"+"\t工作部门\t"+"工作日期");
			for(int j=0;j<employee.length;j++) {
				if(employee[j].getworkplace().equalsIgnoreCase(dpartment[i].dname)) {
					System.out.println(employee[j].toString());
				}
			}
		}
	}

}