import java.util.Scanner;
public class Cnlder {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		System.out.println("请输入您要查询的年份：");
		int years=input.nextInt();
		System.out.println("请输入您要查询的月份：");
		int month=input.nextInt();
		printCalender(years,month);
	}
	public static boolean isLeap(int year) {//是否 为闰年
		if(year%4!=0) {
			return false;	
		}
		else
		return true;
}
		public static int days(int year,int month) {//某年某月有几天
			int i,day;
			switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				day=31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				day=30;
				break;
				default:
					if(isLeap(year))
					day=29;
					else
					day=28;
			}
			return day; 
		}
		public static int totalDays(int year,int month) {//计算到1900.1.1日的总天数
			int i,sumday=0;
			if(year>1900){
				for(i=1900;i<year;i++){
					if(isLeap(i)){
						sumday+=366;
					}
					else
					sumday+=365;	
				}
			}
			else{
				for(i=year;i<=1900;i++){
					if(isLeap(i)){
						sumday+=366;
					}
					else
					sumday+=365;	
				}
			}
			for(i=1;i<month;i++){
				switch(i){
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					sumday+=31;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					sumday+=30;
					break;
					default:
						if(isLeap(i))
						sumday+=29;
						else
						sumday+=28;
						break;
				}
			}
			return sumday;
			
}
		public static void printCalender(int year,int month) {//dispaly 日历
			int today,ii,times;
			today=totalDays(year,month)%7;
			System.out.println("\t\t\t"+year+"年"+month+"月");
			System.out.println("星期日\t星期一\t星期二\t星期三\t星期四\t星期五\t星期六");
			for(ii=1;ii<=today;ii++){
					System.out.print(" \t");
			}
			times=today;
			for(int i=1;i<=days(year,month);i++){
				times++;	
				System.out.print(i+"\t");
				if(times==7){
					times=0;
					System.out.print("\n");
				}
			}
		}
}