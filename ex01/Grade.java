import java.util.Scanner;
import java.util.Arrays;
public class Grade {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in =new Scanner(System.in);
	//	Arrays arr =new Arrays();
		int peoplescore[][]= new int[5][10];//选手的各个得分
		double ave[]=new double[5];
		int temp;
		for(int i=0;i<5;i++) {
			System.out.println("请输入评分(满分是10分不能给出小于0的得分)");
			for(int j=0;j<10;j++) {
				temp=in.nextInt();
				while(temp<0||temp>10) {
					System.out.println("输入有误请重新输入");
					temp=in.nextInt();
				}
				peoplescore[i][j]=temp;
				}
		}
		int[] max=new int[5];
		int[] min=new int[5];
		max=maxsize(peoplescore);
		for(int i=0;i<5;i++) {
			System.out.println(max[i]);
		}
		min=minsize(peoplescore);
		for(int i=0;i<5;i++) {
			System.out.println(min[i]);
		}
		ave=average(peoplescore,max,min);
		Arrays.sort(ave);
		for(int i=0;i<5;i++) {
			System.out.println("第"+(i+1)+"名\t"+"的选手得分是"+ave[4-i]);
		}
		
	}
	public static int[] maxsize(int[][] score) {//求最大值
		int[] max=new int[5];	
		for(int i=0;i<score.length;i++) {
			max[i]=score[i][0];//忽略了max是一个数组，只定义了max[0]=score[0][0],并放在了循环外面，所以出错。
			for(int j=0;j<score[i].length;j++) {
				if(max[i]<score[i][j]) {
					max[i]=score[i][j];
				}
			}
		}
		return max;
	}
	public static int[] minsize(int[][] score) {//求最小值
		int[] min=new int[5];
		
		for(int i=0;i<score.length;i++) {
			min[i]=score[i][0];//问题同max
			for(int j=0;j<score[i].length;j++) {
				if(min[i]>score[i][j]) {
					min[i]=score[i][j];
				}
			}
		}
		return min;
	}
	public static double[] average(int[][] score,int[] max,int[] min) {//求平均分
		double ave[]=new double[5];
		double sum=0;
		for(int i=0;i<score.length;i++) {
			
			for(int j=0;j<score[i].length;j++) {
				sum+=score[i][j];
			}
			sum=sum-max[i];
			sum=sum-min[i];
			ave[i]=sum/8;
			sum=0;
		}
		return ave;
	}
}
