
public class Bank {
	private static String bankName =  "中国工商银行" ;
	private String name;
	private String password;
	private double balance=0;
	private double turnover;
	public static void welcome(){
		System.out.println("欢迎来到中国工商银行");
	}
	public Bank() {
		
	}
	public Bank(String name,String password,double turnover){//构造方法
		this.name=name;
		this.password=password;
		this.turnover=turnover;
		balance=turnover-10;
	}
	public void deposit(double money){//存款
		turnover=money;
		balance+=money;
	}
	public int withdrawal(String password,double money){//取款	
		if(password.equalsIgnoreCase(this.password)){
			if(money<=balance){
				turnover=money;
				balance-=money;
				return 1;
			}
			else
			return -1;//取款金额>余额
		}
		else
		return 0;//密码错误
	}
	public static void welcomeNext(){
		System.out.println("感谢您的光临，请您带好随身物品，欢迎下次再来！");
	}
	public String getname(){
		return name;
	}
	public double getbalance(){
		return balance;
	}
	public double getturnover(){
		return turnover;
	}
	public void setname(String name){
		this.name=name;
	}
	public void setpassword(String password){
		this.password=password;
	}
}
