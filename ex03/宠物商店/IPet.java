interface IPet{//接口定义
	 abstract String  toString();
	 abstract int getprice();//价格
	 abstract String getpno();//编号 
	 abstract String getpname();//种类
	 abstract String getpkind();//品种
	 abstract int getnum();//数量 
	 abstract public void setnum(int num);//改变数量
}
