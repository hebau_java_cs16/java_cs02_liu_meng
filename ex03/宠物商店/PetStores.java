class PetStores {
	private IPet[] pets;//定义宠物品种数组
	private int snum=0;//指向宠物品种数组最后一位的下标+1，表示宠物品种总量
	private int sum=0;//定义总量
	//定义宠物商店的大小
	public PetStores(int len) {
		if(len>0) {//判断len的长度，开辟数组空间
			this.pets=new IPet[len];
		}
		else {
			this.pets=new IPet[1];//至少开辟一个空间
		}
	}
	//放入宠物
	public int setpet(IPet pets) {//放入成功返回1,否则返回0
		if(snum==this.pets.length) {
			return 0;//空间已满
		}
		else {
			this.pets[snum]=pets;//放入一个品种的宠物
			snum++;
			sum+=pets.getnum();
			return 1;
		}
	}
	//展示宠物，调用toSting方法即可
	public void display() {
		System.out.println("编号\t种类\t品种\t数量\t价格");
		for(int i=0;i<snum;i++) {
			if(pets[i]!=null)
			System.out.println(this.pets[i].toString());
		}
		System.out.println("--------------------------------------");
		System.out.println("                           共: "+sum+"只");
	}
	//购买宠物
	public IPet buyone(String pno) {
		IPet buypet = null;
		for(int i=0;i<snum;i++) {
			if(pets[i].getpno().equalsIgnoreCase(pno)) {//找到匹配值
				buypet=pets[i];//得到此宠物
				if(pets[i].getnum()==1) {
					for(int j=i;j<snum-1;j++) {//已购买完此宠物，需要变动对象数组，注意：考虑数组下标越界问题
						pets[j]=pets[j+1];
					}
					snum--;//改变宠物总品种数量
				}
				else {
					pets[i].setnum(pets[i].getnum()-1);
				}
				sum--;//改变宠物总量
				return buypet;//返回该宠物
			}
		}
		return buypet;//未找到，不存在
	}
	public void displaybuy(IPet buy[],int num) {//展示购买清单
		int sumprice=0;
		System.out.println("购买的品种以及价格:");
		for(int i=0;i<buy.length;i++) {
			System.out.println(buy[i].getpkind()+"\t"+buy[i].getprice());
			sumprice+=buy[i].getprice();
		}
		System.out.println("购买数量:"+num);
		System.out.println("--------------------------------------");
		System.out.println("                        总价格="+sumprice);
	}
	//关键字查询
	public IPet[] search(String keykind) {//关键字查询
		IPet[] pets = null;//查询结果未知，不确定有几个
		int len = 0;
		for(int i=0;i<this.snum;i++) {//确定查询结果数组的大小
			if(this.pets[i].getpname().indexOf(keykind)!=-1||this.pets[i].getpkind().indexOf(keykind)!=-1) {//查看关键字在宠物名和宠物种类中是否存在，indexOf(String str)函数返回-1为未找到
				len++;
			}
		}//错误：用了equalsIgnoreCase方法，原因：关键字指信息中存在这部分就可以，不必完全等于
		if(len>0) {//如果查询结果存在
			pets = new IPet[len];
			for(int i=0,j=0;i<this.snum;i++) {
				if(this.pets[i].getpname().indexOf(keykind)!=-1||this.pets[i].getpkind().indexOf(keykind)!=-1) {//查看关键字在宠物名和宠物种类中是否存在，indexOf(String str)函数返回-1为未找到
					pets[j]=this.pets[i];
					j++;
				}
			}
			return pets;
		}
		else {
			return null;
		}
	}
}
