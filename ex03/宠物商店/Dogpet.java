class Dogpet implements IPet{//宠物狗
	private int price;//价格
	private String pno;//编号
	private String pname;//种类
	private String pkind;//品种
	private int num;//数量
	public Dogpet(int price,String pno,String pname,String pkind,int num) {//构造方法
		this.price=price;
		this.pno=pno;
		this.pname=pname;
		this.pkind=pkind;
		this.num=num;
	}
	//相应的seter和geter方法
	public void setprice(int price) {
		this.price=price;
	}
	public int getprice() {
		return price;
	}
	public void setpno(String pno) {
		this.pno=pno;
	}
	public String getpno() {
		return pno;
	}
	public void setpname(String pname) {
		this.pname=pname;
	}
	public String getpname() {
		return pname;
	}
	public void setpkind(String pkind) {
		this.pkind=pkind;
	}
	public String getpkind() {
		return pkind;
	}
	public void setnum(int num) {
		this.num=num;
	}
	public int getnum() {
		return num;
	}
	public String toString() {//返回基本信息
		return pno+'\t'+pname+'\t'+pkind+'\t'+num+'\t'+price;//返回 编号、种类、品种、数量、价格
	}
}
