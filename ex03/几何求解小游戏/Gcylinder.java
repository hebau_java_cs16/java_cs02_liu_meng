
public class Gcylinder extends Graphic{//Բ����
	public Gcylinder(int r,int high) {
		super.sethigh(high);
		super.setr(r);
	}
	public double volume(Graphic a) {
		return a.getr()*a.getr()*Math.PI*a.gethigh();
	}
	public double area(Graphic a) {
		return a.getr()*a.getr()*Math.PI+2*a.getr()*Math.PI*a.gethigh();
	}
}
