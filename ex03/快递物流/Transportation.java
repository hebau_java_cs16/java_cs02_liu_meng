package kuaidi;

public abstract class Transportation {
	private String bno;
	private String xno;
	private String name;
	public abstract void transport();//抽象方法
	public Transportation() {//无参构造方法
		
	}
	public Transportation(String bno,String xno,String name) {//三个参数的构造方法
		this.bno=bno;
		this.xno=xno;
		this.name=name;
	}
	public void setbno(String bno) {//bno的set方法
		this.bno=bno;
	}
	public void setxno(String xno) {//xno的set方法
		this.xno=xno;
	}
	public void setname(String name) {//name的set方法
		this.name=name;
	}
	public String getbno() {//编号的get方法
		return bno;
	}
	public String getxno() {//型号的get方法
		return xno;
	}
	public String getname() {//运输负责人的get方法
		return name;
	}
}
