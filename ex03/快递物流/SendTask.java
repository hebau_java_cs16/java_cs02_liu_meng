package kuaidi;

public class SendTask {//快递任务类
	private String number;//快递单号
	private double weight;//快递重量
	public SendTask() {//无参构造方法
		
	}
	public SendTask(String number,double weight) {//含参构造方法
		this.number=number;
		this.weight=weight;
	}
	public void sendBefore() {//输出运输前的检查信息和快递单号
		System.out.println("订单开始处理，仓库开始验货.....");
		System.out.println("快递单号："+number+" 快递重量："+weight+"kg");
	}
	public void send(Transportation t,GPS tool) {
		t.transport();//输出运输信息
		tool.showCoordinate();//输出位置信息
	}
	public void sendAfter(Transportation t) {
		System.out.println("您的货物已送达，请您验收。");
	}
	public void setnumber(String number) {
		this.number=number;
	}
	public void setweight(int weight) {
		this.weight=weight;
	}
	public double getweight() {
		return weight;
	}
	public String getnumber() {
		return number;
	}
}
