package kuaidi;

public class TDTransportation extends Transportation{
	public TDTransportation(String bno,String xno,String name){
		super(bno,xno,name);
	}
	public void transport() {
		System.out.println("您的货物正由编号为"+getbno()+",型号为"+getxno()+"的专用运输车运输,"+"负责人："+getname());
	}
}
