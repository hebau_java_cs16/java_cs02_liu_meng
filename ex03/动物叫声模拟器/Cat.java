
public class Cat implements Animal{
	private String name;
	public Cat(String name) {//构造方法
		this.name=name;
	}
	public void cry() {//叫声
		System.out.println("喵喵喵！");
	}
	public void getAnimalName() {//种类名称
		System.out.print(name);
	}
}
