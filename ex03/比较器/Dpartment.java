package chageshiyan3_4;
public class Dpartment {
	String Dno;
	String dname;
	Employee manager = null;
	public Dpartment(String Dno,String dname,Employee manager) {//定义构造方法
		this.Dno=Dno;
		this.dname=dname;
		this.manager=manager;
	}
	public String toString() { //输出基本部门信息
		return "部门编号"+Dno+"\t"+"部门名称"+dname+"\t"+"经理编号"+manager.getEno();
	}
	
}