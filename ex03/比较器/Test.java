package chageshiyan3_4;//问题：排序没有作用,已改正

import java.util.Arrays;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;//日期格式的转换
import java.util.Scanner;
import java.util.Date;
public class Test {

	public static void main(String[] args) throws Exception {//忽略了throws Exception 
		Scanner in = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//月份忘记了大写，导致排序出现三个相同年份的生日就会出错
		Employee[] employee = new Employee[10];//对象数组中的每个元素初始化都需要单独将其实例化，因为每一个都是一个对象
		//单独实例化
		employee[0] = new Employee("101","李华","男",sdf.parse("1988-10-25"),"开发部",sdf.parse("2001-06-08"));
		employee[1] = new Employee("102","魏紫萱","男",sdf.parse("1988-03-05"),"市场部",sdf.parse("2001-06-01"));
		employee[2] = new Employee("103","赵欣茜","女",sdf.parse("1997-01-07"),"开发部",sdf.parse("2018-01-09"));
		employee[3] = new Employee("104","刘萌","男",sdf.parse("1998-12-23"),"开发部",sdf.parse("2019-08-09"));
		employee[4] = new Employee("105","张楠","男",sdf.parse("1989-05-15"),"市场部",sdf.parse("2002-07-26"));
		employee[5] = new Employee("106","小明","男",sdf.parse("1986-04-28"),"市场部",sdf.parse("2000-10-23"));
		employee[6] = new Employee("109","李嵘","女",sdf.parse("1998-11-21"),"市场部",sdf.parse("2019-08-09"));
		employee[7] = new Employee("100","赵欣","女",sdf.parse("1994-07-10"),"开发部",sdf.parse("2018-07-25"));
		employee[8] = new Employee("110","万茜","男",sdf.parse("1990-03-24"),"市场部",sdf.parse("2011-09-19"));
		employee[9] = new Employee("153","小红","女",sdf.parse("1988-12-17"),"开发部",sdf.parse("2019-09-15"));
		Dpartment[] dpartment = new Dpartment[2];//部门类
		dpartment[0]=new Dpartment("001",employee[0].getworkplace(),employee[0]);
		dpartment[1]=new Dpartment("002",employee[1].getworkplace(),employee[1]);
		Arrays.sort(employee);
		System.out.println(dpartment[0].toString());
		System.out.println(dpartment[1].toString());
		System.out.println("员工编号\t"+"员工姓名\t"+"性别\t"+"生日\t"+"\t工作部门\t"+"工作日期");
		for(int j=0;j<employee.length;j++) {			
			System.out.println(employee[j].toString());
		}
		System.out.println("====================");
		Arrays.sort(employee, new Birthcomparator());
		for(int j=0;j<employee.length;j++) {			
			System.out.println(employee[j].toString());
		}
	}
}
