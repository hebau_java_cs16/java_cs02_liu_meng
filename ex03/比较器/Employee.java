package chageshiyan3_4;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;//日期格式的转换
public class Employee implements Comparable<Employee>{
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
	private String Eno=null;
	private String name = null;
	private String sex= null;
	private Date brithday= null;
	private String workplace = null;
	private Date worktime = null;
	public Employee() {
		
	}
	public Employee(String Eno,String name,String sex,Date brithday,String workplace,Date worktime){
		this.Eno=Eno;
		this.name=name;
		this.sex=sex;
		this.brithday=brithday;
		this.workplace=workplace;
		this.worktime=worktime;
	}
	public void setEno(String Eno) {
		this.Eno=Eno;
	}
	public String getEno() {
		return Eno;
	}
	public void setname(String name) {
		this.name=name;
	}
	public String getname() {
		return name;
	}
	public void setsex(String sex) {
		this.sex=sex;
	}
	public String getsex() {
		return sex;
	}
	public void setworkplace(String workplace) {
		this.workplace=workplace;
	}
	public String getworkplace() {
		return workplace;
	}
	public void setbrithday(Date birthday) {
		this.brithday=birthday;
	}
	public Date getbrithday() {
		return brithday;
	}
	public void setworktime(Date worktime) {
		this.worktime=worktime;
	}
	public Date getworktime() {
		return worktime;
	}
	public String toString() {
		return Eno+"\t"+name+"\t"+sex+"\t"+sdf.format(brithday)+"\t"+workplace+"\t"+sdf.format(worktime);
	}
	public int compareTo(Employee o) {
		if(o.getbrithday().getTime()>this.getbrithday().getTime()) {
			return -1;
		}else if(o.getbrithday().getTime()<this.getbrithday().getTime()) {
			return 1;
		}else {
			return 0;
		}
	}
}

