
public class Feeder {
	String name;
	public Feeder(String name) {
		this.name=name;
	}
	public void feedlion(Animal lion) {
		lion.eat(lion);
	}
	public void feedmonkey(Animal monkey) {
		monkey.eat(monkey);
	}
	public void feedpigeon(Animal pigeon) {
		pigeon.eat(pigeon);
	}
	public void feedAnimal(Animal[] animal) {
		for(int i = 0;i<animal.length;i++) {
			System.out.println(animal[i].getname()+animal[i].getfood());
		}
	}
	public String getname() {
		return name;
	}
}
