
public abstract class Animal {
	private String name;
	private String food;
	public abstract void eat(Animal animal);
	public abstract void feedanimal(Animal animal[]);
	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name=name;
	}
	public String getfood() {
		return food;
	}
	public void setfood(String food) {
		this.food=food;
	}
}
