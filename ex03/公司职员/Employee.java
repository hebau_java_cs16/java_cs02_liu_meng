
public class Employee {
	private String name;
	private int age;
	private String sex;
	public Employee(String name,int age,String sex) {//含参构方法
		this.name=name;
		this.age=age;
		this.sex=sex;
	}
	public Employee(){//无参构造方法
		
	}
	public String getname() {
		return name;
	}
	public int getage() {
		return age;
	}
	public String getsex() {
		return sex;
	}
	public void setname(String name) {
		this.name=name;
	}
	public void setage(int age) {
		this.age=age;
	}
	public void setsex(String sex) {
		this.sex=sex;
	}
	public String toString(Employee a) {
		return "姓名:"+a.name+"\t年龄\t"+a.age+"性别\t"+a.sex;
	}
}
