
public class Test2 {
	public static void main(String[] args) {//未使用向上转型
		Manage[] manage = new Manage[2];
		manage[0] = new Manage("赵茜",25,"女","市场部经理",400000);
		manage[1] = new Manage("李华",28,"男","开发部经理",450000);
		Worker worker[] = new Worker[6];
		worker[0] = new Worker("杜名",27,"男","开发部",5000);
		worker[1] = new Worker("刘敏敏",26,"女","市场部",5000);
		worker[2] = new Worker("魏姿",24,"女","开发部",5000);
		worker[3] = new Worker("杨彭涛",22,"男","市场部",5000);
		worker[4] = new Worker("张楠",25,"男","市场部",5000);
		worker[5] = new Worker("赵西西",23,"女","开发部",5000);
		System.out.println("姓名\t年龄\t性别\t部门\t工资");
		for(int i=0;i<manage.length;i++)
			System.out.println(manage[i].toString()+"(年薪)");
		for(int j=0;j<worker.length;j++)
			System.out.println(worker[j].toString()+"(月薪)");
	}
}
