
public class Worker extends Employee{
	private String department;
	private int monthlypay;
	public Worker(String name,int age,String sex,String department,int monthlypay) {
		super(name,age,sex);
		this.department=department;
		this.monthlypay=monthlypay;
	}
	public String getdepartment() {
		return department;
	}
	public int getmonthlypay() {
		return monthlypay;
	}
	public void setdepartment(String department) {
		this.department=department;
	}
	public void setmonthlypay(int monthlypay) {
		this.monthlypay=monthlypay;
	}
	public String toString() {
		return super.getname()+"\t"+super.getage()+"\t"+super.getsex()+"\t"+this.department+"\t"+this.monthlypay;
	}
	public String toString(Worker a) {
		return a.getname()+"\t"+a.getage()+"\t"+a.getsex()+"\t"+a.department+"\t"+a.monthlypay;
	}
}
