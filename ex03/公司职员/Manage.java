
public class Manage extends Employee {
	private String post;
	private long money;
	public Manage(String name,int age,String sex,String post,long money) {
		super(name,age,sex);
		this.money=money;
		this.post=post;
	}
	public String getpost() {
		return post;
	}
	public void setpost(String post) {
		this.post=post;
	}
	public void setmoney(long money) {
		this.money=money;
	}
	public long getmoney() {
		return money;
	}
	public String toString() {
		return super.getname()+"\t"+super.getage()+"\t"+super.getsex()+"\t"+this.post+"\t"+this.money;
	}
	public String toString(Manage a) {
		return a.getname()+"\t"+a.getage()+"\t"+a.getsex()+"\t"+a.post+"\t"+a.money;
	}
}
